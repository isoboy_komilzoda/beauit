import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly loginUrl = 'http://kremnica.beauit.com/api/v1/login';

  constructor(private http: HttpClient) { }

  login(data) {
    return this.http.post(this.loginUrl, data);
  }
}
