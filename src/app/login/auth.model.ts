export interface User {
  guid: string;
  userName: string;
  name?: any;
  surname?: any;
  email: string;
  phone?: any;
  shippingAddress?: any;
  shippingMethod?: any;
  paymentMethod?: any;
}

export interface UserModel {
  token: string;
  user: User;
}
