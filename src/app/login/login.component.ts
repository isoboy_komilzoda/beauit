import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from './auth.service';
import {UserModel} from './auth.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  constructor(private auth: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.form =  new FormGroup({
      'username': new FormControl('', Validators.required),
      'password': new FormControl('', Validators.required)
    });
  }

  onSubmit() {
    this.auth.login(this.form.value)
      .subscribe((res: UserModel) => {
        if (res) {
          localStorage.setItem('guid', res.user.guid);
          localStorage.setItem('token', res.token);

          this.router.navigate(['/profile/settings']);
        }
      });
  }

}
