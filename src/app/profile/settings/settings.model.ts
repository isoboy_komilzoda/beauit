export interface City {
  id: number;
  name: string;
}

export interface ShippingAddress {
  id: number;
  city: City;
  street: string;
  building: string;
  block: string;
  room: string;
  postalCode: string;
  main: boolean;
}

export interface Shipment {
  id: number;
  name: string;
  selected: boolean;
  remove: boolean;
}

export interface Payment {
  id: number;
  name: string;
  selected: boolean;
  remove: boolean;
}

export interface AvailableMethods {
  shipment: Shipment[];
  payment: Payment[];
}
