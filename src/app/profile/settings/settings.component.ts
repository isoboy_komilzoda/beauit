import {Component, OnDestroy, OnInit} from '@angular/core';
import {SettingsService} from './settings.service';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, mergeMap, switchMap, takeUntil} from 'rxjs/operators';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AvailableMethods, Payment, Shipment} from './settings.model';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit, OnDestroy {

  form: FormGroup;
  searchTextChanged = new Subject<string>();
  payment: Payment[];
  shipment: Shipment[];
  cities;
  cityId;
  showListOfCity = false;
  private $destroy = new Subject();

  constructor(private settings: SettingsService) { }

  ngOnInit() {
    this.initForm();
    this.getPaymentMethods();
    this.searchTextChanged
      .pipe(debounceTime(500),
      distinctUntilChanged(),
      mergeMap(search => this.settings.getCity(search)))
      .subscribe(res => {
        this.showListOfCity = false;
        this.cities = res;
      });
  }

  getPaymentMethods() {
    this.settings.getPaymentMethods()
      .pipe(takeUntil(this.$destroy))
      .subscribe((res: AvailableMethods) => {
        this.payment = res.payment;
        this.shipment = res.shipment;
      });
  }

  initForm() {
    this.form =  new FormGroup({
      'city': new FormControl('', Validators.required),
      'street': new FormControl('', Validators.required),
      'building': new FormControl('', Validators.required),
      'postalCode': new FormControl('', Validators.required),
      'block': new FormControl(''),
      'room': new FormControl(''),
      'main': new FormControl(true),
      'paymentMethod': new FormControl('', Validators.required),
      'shipmentMethod': new FormControl('', Validators.required)
    });
  }

  onSearch(e) {
    this.searchTextChanged.next(e.target.value);
  }

  selectCity(city) {
    this.showListOfCity = true;
    this.cityId = city.id;
    this.form.get('city').patchValue(city.name);
  }

  onSubmit() {
    const data = this.form.value;
    data['id'] = this.cityId;
    let address;
    if (data.main) {
      address = this.settings.newShippingAddress(data);
    } else {
      address = this.settings.defaultShippingAddress(data);
    }
    address.pipe(
      takeUntil(this.$destroy),
      switchMap(() => this.settings.favoriteMethod(data.paymentMethod, 'payment')),
      switchMap(() => this.settings.favoriteMethod(data.shipmentMethod, 'shipment'))
    ).subscribe(res => console.log(res));
  }

  ngOnDestroy() {
    this.$destroy.next(true);
    this.$destroy.unsubscribe();
  }

}
