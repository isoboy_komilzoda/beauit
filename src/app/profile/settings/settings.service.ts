import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AvailableMethods, ShippingAddress} from './settings.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  guid;
  private readonly cityUlr = 'http://kremnica.beauit.com/api/v1/geo/cities/';
  private readonly paymentUrl = 'http://kremnica.beauit.com/api/v1/user/';
  private readonly newShippingAddressUrl = 'http://kremnica.beauit.com/api/v1/user/';
  private readonly favoriteMethodUrl = 'http://kremnica.beauit.com/api/v1/user/';

  constructor(private http: HttpClient) {
    this.guid = localStorage.getItem('guid');
  }

  getCity(search) {
    return this.http.get(this.cityUlr + search);
  }

  getPaymentMethods(): Observable<AvailableMethods> {
    const header = this.getHeader();
    return this.http.get<AvailableMethods>(this.paymentUrl + this.guid + '/methods', {headers : header});
  }

  newShippingAddress(data: ShippingAddress) {
    const header = this.getHeader();
    const formData = this.serverFormData(data);
    formData.append('main', String(data.main));
    return this.http.post<ShippingAddress>(this.newShippingAddressUrl + this.guid + '/shipping-address', formData, {headers : header});
  }

  defaultShippingAddress(data: ShippingAddress) {
    const header = this.getHeader();
    const formData = this.serverFormData(data);
    return this.http.post<ShippingAddress>(this.newShippingAddressUrl + this.guid + '/shipping-address/default', formData, {headers : header});
  }

  serverFormData(data) {
    const formData = new FormData();
    formData.append('city', String(data.id));
    formData.append('street', data.street);
    formData.append('building', data.building);
    formData.append('block', data.block);
    formData.append('room', data.room);
    formData.append('postalCode', data.postalCode);

    return formData;
  }

  favoriteMethod(data, type): Observable<any> {
    const formData = new FormData();
    formData.append('methodId', data);
    const header = this.getHeader();
    return this.http.post(this.favoriteMethodUrl + `${this.guid}/method/${type}`, formData, {headers: header});
  }

  private getHeader() {
    const token = localStorage.getItem('token');
    return new HttpHeaders({
      'x-api-key': token
    });
  }
}
